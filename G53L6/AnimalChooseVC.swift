//
//  AnimalChooseVC.swift
//  G53L6
//
//  Created by Ivan Vasilevich on 3/23/17.
//  Copyright © 2017 Smoosh Labs. All rights reserved.
//

import UIKit

class AnimalChooseVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .lightGray
        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = DataManager.shared.globalString
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let button = sender as? UIButton {
            if let vc = segue.destination as? ViewController {
                vc.prevControllerData = "yazz"
            }
            segue.destination.title = button.currentTitle
        }
    }

}
