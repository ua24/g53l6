//
//  ViewController.swift
//  G53L6
//
//  Created by Ivan Vasilevich on 3/23/17.
//  Copyright © 2017 Smoosh Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageBox: UIImageView!
    var prevControllerData = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        log()
    }
    
    @IBOutlet weak var textInputField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        log()
        //        var name = "Ivan"
        //        let str = name
        //        name = "Vasilevich"
        //        print(name)
        //        print(str)
        //        log("end")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        log()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        log()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        log()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        log()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        log()
        DataManager.shared.globalString = textInputField.text!
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        log()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        var imageName = ""
//        if arc4random()%2 == 1 {
//            imageName = "cats"
//        }
//        else {
//            imageName = "cats2"
//        }
//        let image = UIImage(named: imageName)
//        imageBox.image = image
        
        imageBox.image = UIImage(named: arc4random()%2 == 1 ? "cats" : "cats2")
    }
    
    @IBAction func showGreenVC() {
        performSegue(withIdentifier: "toGreen", sender: nil)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        log()
//        
//        if let wishPreviewVC = segue.destination as? WishPreviewVC {
//            wishPreviewVC.customField = "mySupWish12"
//            
//        }
//        
//    }
    
    //    func log(logMessage: String, functionName: String = __FUNCTION__) {
    //        print("\(functionName): \(logMessage)")
    //    }
    func log(_ functionName: String = #function) {
        print("\(functionName)")
    }
    
}

